//package com.masiabdoos.viesure.di
//
//import android.content.Context
//import androidx.room.Room
//import com.masiabdoos.viesure.data.database.VieSureDataBase
//import dagger.Module
//import dagger.Provides
//import dagger.hilt.InstallIn
//import dagger.hilt.android.qualifiers.ApplicationContext
//import dagger.hilt.components.SingletonComponent
//import javax.inject.Named
//
//@Module
//@InstallIn(SingletonComponent::class)
//class TestAppModule {
//
//    @Provides
//    @Named("DataBaseTest")
//    fun injectInMemoryRoom(@ApplicationContext context: Context) =
//        Room.inMemoryDatabaseBuilder(context, VieSureDataBase::class.java)
//            .allowMainThreadQueries()
//            .build()
//
////    @Provides
////    @Singleton
////    @Named("fakeToDoRepo")
////    fun provideBookRemoteDataSource(): BookRemoteDataSource {
////        return BookRemoteDataSourceImplTest
////    }
//
//
//}