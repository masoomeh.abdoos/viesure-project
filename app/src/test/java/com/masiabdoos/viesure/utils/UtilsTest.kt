package com.masiabdoos.viesure.utils

import com.google.common.truth.Truth
import org.junit.Before
import org.junit.Test


class UtilsTest {

//    lateinit var utils: Utils

    val date1 = "8/30/1932"
    val date2 = "6/5/1967"
    val date3 = "11/1/1880"
    val date4 = "800 BC"
    val date5 = "1320"
    val date6 = "12/1/1856"
    val date7 = "7/11/1960"
    val date8 = "6/8/1949"
    val date9 = "1/28/1813"
    val date10 = "10/18/1851"

    @Before
    fun setup() {

    }

    @Test
    fun reformatDateInvalidTest() {

        Truth.assertThat(Utils.parseStringDateToLong(date4))
            .isEqualTo(0)

        Truth.assertThat(Utils.parseStringDateToLong(date5))
            .isEqualTo(0)

        Truth.assertThat(Utils.parseStringDateToLong("ad"))
            .isEqualTo(0)

        Truth.assertThat(Utils.parseStringDateToLong(null))
            .isEqualTo(0)

        Truth.assertThat(Utils.parseStringDateToLong("null"))
            .isEqualTo(0)

        Truth.assertThat(Utils.parseStringDateToLong(""))
            .isEqualTo(0)
    }

    @Test
    fun parseStringDateToLongValidTest() {

        Truth.assertThat(Utils.parseStringDateToLong(date1))
            .isEqualTo(-1178323200000)

        Truth.assertThat(Utils.parseStringDateToLong(date2))
            .isEqualTo(-81302400000)

        Truth.assertThat(Utils.parseStringDateToLong(date3))
            .isEqualTo(-2813788800000)

        Truth.assertThat(Utils.parseStringDateToLong(date6))
            .isEqualTo(-3568579200000)

        Truth.assertThat(Utils.parseStringDateToLong(date7))
            .isEqualTo(-299030400000)

        Truth.assertThat(Utils.parseStringDateToLong(date8))
            .isEqualTo(-649036800000)

        Truth.assertThat(Utils.parseStringDateToLong(date9))
            .isEqualTo(-4952102400000)

        Truth.assertThat(Utils.parseStringDateToLong(date10))
            .isEqualTo(-3730233600000)
    }

    @Test
    fun reformatDateInValidTest() {
        Truth.assertThat(Utils.reformatDate("1wsd"))
            .isEqualTo("")

        Truth.assertThat(Utils.reformatDate(null))
            .isEqualTo("")

        Truth.assertThat(Utils.reformatDate("null"))
            .isEqualTo("")

        Truth.assertThat(Utils.reformatDate(""))
            .isEqualTo("")

        Truth.assertThat(Utils.reformatDate(date4))
            .isEqualTo("")

        Truth.assertThat(Utils.reformatDate(date5))
            .isEqualTo("")
    }

    @Test
    fun reformatDateValidTest() {

        Truth.assertThat(Utils.reformatDate(date1))
            .isEqualTo("08/30/1932")

        Truth.assertThat(Utils.reformatDate(date2))
            .isEqualTo("06/05/1967")

        Truth.assertThat(Utils.reformatDate(date3))
            .isEqualTo("11/01/1880")

        Truth.assertThat(Utils.reformatDate(date6))
            .isEqualTo("12/01/1856")

        Truth.assertThat(Utils.reformatDate(date7))
            .isEqualTo("07/11/1960")

        Truth.assertThat(Utils.reformatDate(date8))
            .isEqualTo("06/08/1949")

        Truth.assertThat(Utils.reformatDate(date9))
            .isEqualTo("01/28/1813")

        Truth.assertThat(Utils.reformatDate(date10))
            .isEqualTo("10/18/1851")
    }


    @Test
    fun parseLongToFormattedDateStringInValidTest() {

        Truth.assertThat(Utils.parseLongToFormattedDateString(0))
            .isEqualTo("")

    }

    @Test
    fun parseLongToFormattedDateStringValidTest() {

        Truth.assertThat(Utils.parseLongToFormattedDateString(-1178323200000))
            .isEqualTo("Tue, Aug 30, '32")

        Truth.assertThat(Utils.parseLongToFormattedDateString(-1178323200000))
            .isEqualTo("Tue, Aug 30, '32")

    }
}