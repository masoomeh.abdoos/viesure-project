package com.masiabdoos.viesure.data.repository.book

import app.cash.turbine.test
import com.google.common.truth.Truth
import com.masiabdoos.viesure.data.database.dao.BookDao
import com.masiabdoos.viesure.data.map.BookApiToBookEntityMapper
import com.masiabdoos.viesure.data.map.BookApiToBookMapper
import com.masiabdoos.viesure.data.map.BookDetailsMapper
import com.masiabdoos.viesure.data.map.BookMapper
import com.masiabdoos.viesure.data.network.api.FakeViesureServiceApi
import com.masiabdoos.viesure.data.network.model.BookApiModel
import com.masiabdoos.viesure.data.network.model.Response
import com.masiabdoos.viesure.data.source.local.BookLocalDataSource
import com.masiabdoos.viesure.data.source.local.BookLocalDataSourceImpl
import com.masiabdoos.viesure.data.source.remote.BookRemoteDataSource
import com.masiabdoos.viesure.data.source.remote.BookRemoteDataSourceImpl
import com.masiabdoos.viesure.data.source.remote.FakeFailedDataSourceImplTest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class BookRepositoryImplTest {

    private lateinit var fakeSuccessfulBookRepository: BookRepository

    private lateinit var fakeFailedBookRepository: BookRepository

    @Mock
    private lateinit var fakeSuccessBookRemoteDataSource: BookRemoteDataSource

    @Mock
    private lateinit var fakeFailedBookRemoteDataSource: BookRemoteDataSource

    @Mock
    private lateinit var fakeSuccessBookRemoteDataSourceImpl: BookRemoteDataSource

    private lateinit var fakeSuccessBookLocalDataSource: BookLocalDataSource

    private lateinit var fakeSuccessfulViesureServiceApi: FakeViesureServiceApi

    @Mock
    private lateinit var bookDao: BookDao

    @Mock
    private lateinit var bookMapper: BookMapper

    @Mock
    private lateinit var bookApiModel: BookApiModel


    @Mock
    private lateinit var bookApiToBookEntityMapper: BookApiToBookEntityMapper

    @Mock
    private lateinit var bookAoiToBookMapper: BookApiToBookMapper

    @Mock
    private lateinit var bookDetailsMapper: BookDetailsMapper


    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)

        fakeSuccessfulViesureServiceApi = FakeViesureServiceApi()
        fakeSuccessBookRemoteDataSourceImpl =
            BookRemoteDataSourceImpl(fakeSuccessfulViesureServiceApi)
        fakeSuccessBookLocalDataSource = BookLocalDataSourceImpl(bookDao)

        fakeSuccessfulBookRepository = BookRepositoryImpl(
            fakeSuccessBookRemoteDataSourceImpl,
            fakeSuccessBookLocalDataSource,
            bookMapper,
            bookApiToBookEntityMapper,
            bookAoiToBookMapper,
            bookDetailsMapper
        )

        fakeFailedBookRemoteDataSource = FakeFailedDataSourceImplTest()
        fakeFailedBookRepository = BookRepositoryImpl(
            fakeFailedBookRemoteDataSource,
            fakeSuccessBookLocalDataSource,
            bookMapper,
            bookApiToBookEntityMapper,
            bookAoiToBookMapper,
            bookDetailsMapper
        )

    }

    @Test
    fun returnSuccessResponseWithRemoteDataSource() {
        runBlocking {
            Mockito.`when`(fakeSuccessBookRemoteDataSource.getBooks())
                .thenReturn(flowOf(Response.Success(listOf(bookApiModel))))

            fakeSuccessfulBookRepository.getBooks().test {
                val response = awaitItem()
                Truth.assertThat(response).isInstanceOf(Response.Success::class.java)
                awaitComplete()
            }

        }
    }

    @Test
    fun returnFailedResponseWithRemoteDataSource() {
        runBlocking {
            Mockito.`when`(fakeSuccessBookRemoteDataSource.getBooks()).thenThrow(RuntimeException())

            fakeFailedBookRepository.getBooks().test {
                val response = awaitItem()
                print("response: $response")
                Truth.assertThat(response).isInstanceOf(Response.Error::class.java)
                awaitComplete()
            }

        }
    }

}
