//package com.masiabdoos.viesure.data.source.local
//
//import com.google.common.truth.Truth
//import com.masiabdoos.viesure.data.network.api.ViesureServiceApi
//import kotlinx.coroutines.ExperimentalCoroutinesApi
//import kotlinx.coroutines.runBlocking
//import org.junit.Before
//import org.junit.Rule
//import org.junit.Test
//import org.mockito.Mock
//import org.mockito.Mockito
//import org.mockito.MockitoAnnotations
//
//class BookLocalDataSourceImplTest {
//
//    private lateinit var bookLocalDataSourceImpl: BookLocalDataSourceImpl
//    @Mock
//    private lateinit var serviceApi: ViesureServiceApi
//
//
//    @ExperimentalCoroutinesApi
//    @get:Rule
//    var mainCoroutineRule = MainCoroutineRule()
//
//    @Before
//    fun setup() {
//
//        MockitoAnnotations.openMocks(this)
//        bookLocalDataSourceImpl = BookLocalDataSourceImpl(serviceApi)
//        Mockito.`when`(mockCommentMapper.map(mockComment)).thenReturn(mockStorytelComment)
//    }
//
//    @Test
//    fun `return successful response`() {
//
//        runBlocking {
//            Mockito.`when`(serviceApi.getComments(POST_ID)).thenReturn(comments)
//            val postsResponse = commentsRepository.getComments(POST_ID)
//            Truth.assertThat(postsResponse is Response.Success).isTrue()
//        }
//    }
//
//    @Test
//    fun `return failed response`() {
//
//        runBlocking {
//            Mockito.`when`(serviceApi.getComments(POST_ID)).thenThrow(RuntimeException())
//            val postsResponse = commentsRepository.getComments(POST_ID)
//            Truth.assertThat(postsResponse is Response.Error).isTrue()
//        }
//    }
//}