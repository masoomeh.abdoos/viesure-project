package com.masiabdoos.viesure.data.source.remote

import app.cash.turbine.test
import com.google.common.truth.Truth
import com.masiabdoos.viesure.data.network.api.ViesureServiceApi
import com.masiabdoos.viesure.data.network.model.BookApiModel
import com.masiabdoos.viesure.data.network.model.Response
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
@RunWith(JUnit4::class)
class BookRemoteDataSourceImplTest {

//    val exceptions = mutableListOf<Throwable>()
//    val customCaptor = CoroutineExceptionHandler { ctx, throwable ->
//        exceptions.add(throwable) // add proper synchronization if the test is multithreaded
//    }

    private lateinit var bookRemoteDataSourceImpl: BookRemoteDataSourceImpl

    @Mock
    private lateinit var serviceApi: ViesureServiceApi

    @Mock
    private lateinit var bookApiModel: List<BookApiModel>


    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        bookRemoteDataSourceImpl = BookRemoteDataSourceImpl(serviceApi)
    }


    @Test
    fun returnErrorResponse() {
        runBlocking {
            `when`(serviceApi.getBooks()).thenThrow(RuntimeException())
            bookRemoteDataSourceImpl.getBooks().test {
                Truth.assertThat(awaitItem()).isInstanceOf(Response.Error::class.java)
                awaitComplete()

            }
        }
    }

    @Test
    fun returnSuccessResponse() {
        runBlocking {

            `when`(serviceApi.getBooks()).thenReturn(bookApiModel)
            bookRemoteDataSourceImpl.getBooks().test {
                Truth.assertThat(awaitItem()).isInstanceOf(Response.Success::class.java)
                awaitComplete()
            }

        }
    }
}
