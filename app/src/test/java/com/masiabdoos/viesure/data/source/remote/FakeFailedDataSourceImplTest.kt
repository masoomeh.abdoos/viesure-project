package com.masiabdoos.viesure.data.source.remote

import com.masiabdoos.viesure.data.network.model.BookApiModel
import com.masiabdoos.viesure.data.network.model.ErrorType
import com.masiabdoos.viesure.data.network.model.Response
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class FakeFailedDataSourceImplTest : BookRemoteDataSource {

    override suspend fun getBooks(): Flow<Response<List<BookApiModel>>> {
        return flow {
            emit(Response.Error(ErrorType.Api.Network))
        }

    }
}