package com.masiabdoos.viesure.data.map

import com.google.common.truth.Truth
import com.masiabdoos.viesure.data.network.model.BookApiModel
import com.masiabdoos.viesure.utils.Utils
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class BookApiToBookEntityMapperTest {

    private lateinit var mapper: BookApiToBookEntityMapper

    @Before
    fun setup() {
        mapper = BookApiToBookEntityMapper()
    }

    @Test
    fun mapDataBaseToDomainLevelOfBookDetails() {

        val bookApiModel = BookApiModel(
            id = 1,
            title = "To Kill a Mockingbird",
            description = "Harper Lee's timeless novel explores the racial injustices of the Deep South through the eyes of young Scout Finch. Set in the 1930s, the story centers around Scout's father, Atticus Finch, a lawyer who defends a black man, Tom Robinson, falsely accused of raping a white woman. The novel delves into themes of racial prejudice, moral growth, and the innocence of childhood. Scout, her brother Jem, and their friend Dill are fascinated by their reclusive neighbor, Boo Radley, who becomes a pivotal character in the story. Through the trial of Tom Robinson, the children witness the harsh realities of racism and injustice. Atticus Finch's unwavering integrity and sense of justice serve as a moral compass for the community. The novel also explores the complexities of social class and the coexistence of good and evil within individuals. Scout's journey from innocence to understanding is a poignant coming-of-age story. The novel's title refers to the idea that it is a sin to kill a mockingbird, symbolizing the destruction of innocence. The narrative is rich with symbolism and explores the importance of empathy and understanding. The character of Boo Radley represents the misunderstood and marginalized individuals in society. The book is a profound commentary on morality and human nature. Harper Lee's masterful storytelling and vivid characters make this a timeless classic. It remains a powerful and relevant exploration of racial and social issues. The novel won the Pulitzer Prize and has been translated into multiple languages.\", \"author\": \"Harper Lee",
            author = "Harper Lee",
            releaseDate = "7/11/1960",
            image = "https://dummyimage.com/366x582.png/5fa2dd/ffffff"
        )
        val bookDetails = mapper.map(bookApiModel)

        Truth.assertThat(bookDetails.id).isEqualTo(bookApiModel.id)
        Truth.assertThat(bookDetails.title).isEqualTo(bookApiModel.title)
        Truth.assertThat(bookDetails.author).isEqualTo(bookApiModel.author)
        Truth.assertThat(bookDetails.releaseDate).isNotEqualTo(bookApiModel.releaseDate)
        Truth.assertThat(bookDetails.releaseDate).isNotEqualTo(bookApiModel.releaseDate)
        Truth.assertThat(bookDetails.releaseDate).isEqualTo(
            Utils.parseStringDateToLong(
                Utils.reformatDate(bookApiModel.releaseDate)
            )
        )
        Truth.assertThat(bookDetails.imageUrl).isEqualTo(bookApiModel.image)
        Truth.assertThat(bookDetails.description).isEqualTo(bookApiModel.description)

    }
}