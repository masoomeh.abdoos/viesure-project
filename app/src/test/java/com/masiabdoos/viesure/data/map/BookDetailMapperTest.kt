package com.masiabdoos.viesure.data.map

import com.google.common.truth.Truth
import com.masiabdoos.viesure.data.database.entities.BookEntity
import com.masiabdoos.viesure.utils.Utils
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class BookDetailMapperTest {

    private lateinit var mapper: BookDetailsMapper

    @Before
    fun setup() {
        mapper = BookDetailsMapper()
    }

    @Test
    fun mapDataBaseToDomainLevelOfBookDetails() {

        val bookEntity = BookEntity(
            id = -5,
            title = "title",
            description = "description",
            author = "name of author",
            releaseDate = -1,
            imageUrl = "image url"
        )
        val bookDetails = mapper.map(bookEntity)

        Truth.assertThat(bookDetails.id).isEqualTo(bookEntity.id)
        Truth.assertThat(bookDetails.title).isEqualTo(bookEntity.title)
        Truth.assertThat(bookDetails.author).isEqualTo(bookEntity.author)
        Truth.assertThat(bookDetails.releaseDate).isNotEqualTo(bookEntity.releaseDate)
        Truth.assertThat(bookDetails.releaseDate).isEqualTo(
            Utils.parseLongToFormattedDateString(bookEntity.releaseDate)
        )
        Truth.assertThat(bookDetails.imageUrl).isEqualTo(bookEntity.imageUrl)
        Truth.assertThat(bookDetails.description).isEqualTo(bookEntity.description)

    }
}