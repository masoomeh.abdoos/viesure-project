package com.masiabdoos.viesure.data.map

import com.google.common.truth.Truth
import com.masiabdoos.viesure.data.database.entities.BookEntity
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class BookMapperTest {

    private lateinit var mapper: BookMapper

    @Before
    fun setup() {
        mapper = BookMapper()
    }

    @Test
    fun mapDataBaseToDomainLevelOfBookDetails() {

        val bookEntity = BookEntity(
            id = 1,
            title = "To Kill a Mockingbird",
            description = "Harper Lee's timeless novel explores the racial injustices of the Deep South through the eyes of young Scout Finch. Set in the 1930s, the story centers around Scout's father, Atticus Finch, a lawyer who defends a black man, Tom Robinson, falsely accused of raping a white woman. The novel delves into themes of racial prejudice, moral growth, and the innocence of childhood. Scout, her brother Jem, and their friend Dill are fascinated by their reclusive neighbor, Boo Radley, who becomes a pivotal character in the story. Through the trial of Tom Robinson, the children witness the harsh realities of racism and injustice. Atticus Finch's unwavering integrity and sense of justice serve as a moral compass for the community. The novel also explores the complexities of social class and the coexistence of good and evil within individuals. Scout's journey from innocence to understanding is a poignant coming-of-age story. The novel's title refers to the idea that it is a sin to kill a mockingbird, symbolizing the destruction of innocence. The narrative is rich with symbolism and explores the importance of empathy and understanding. The character of Boo Radley represents the misunderstood and marginalized individuals in society. The book is a profound commentary on morality and human nature. Harper Lee's masterful storytelling and vivid characters make this a timeless classic. It remains a powerful and relevant exploration of racial and social issues. The novel won the Pulitzer Prize and has been translated into multiple languages.\", \"author\": \"Harper Lee",
            author = "Harper Lee",
            releaseDate = -123456789,
            imageUrl = "https://dummyimage.com/366x582.png/5fa2dd/ffffff"
        )
        val book = mapper.map(bookEntity)

        Truth.assertThat(book.id).isEqualTo(bookEntity.id)
        Truth.assertThat(book.title).isEqualTo(bookEntity.title)
        Truth.assertThat(book.imageUrl).isEqualTo(bookEntity.imageUrl)
        Truth.assertThat(book.description).isEqualTo(bookEntity.description)

    }
}
