package com.masiabdoos.viesure.data.network.api

import com.masiabdoos.viesure.data.network.model.BookApiModel

class FakeViesureServiceApi : ViesureServiceApi {
    override suspend fun getBooks(): List<BookApiModel> {
        return listOf(
            BookApiModel(
                id = 0,
                title = "Moby-Dick",
                description = "description",
                author = "Herman Melville",
                releaseDate = "10/18/1851",
                image = "https://dummyimage.com/600x400.png/00cc00/ffffff"
            ),
            BookApiModel(
                id = 5,
                title = "War and Peace",
                description = "description",
                author = "Leo Tolstoy",
                releaseDate = "1/1/1869",
                image = "https://dummyimage.com/366x582.png/000000/ffffff"
            )
        )
    }
}