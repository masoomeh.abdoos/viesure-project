package com.masiabdoos.viesure.database.dao


import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import app.cash.turbine.test
import com.google.common.truth.Truth
import com.masiabdoos.viesure.data.database.VieSureDataBase
import com.masiabdoos.viesure.data.database.dao.BookDao
import com.masiabdoos.viesure.data.database.entities.BookEntity
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject
import javax.inject.Named

@RunWith(AndroidJUnit4::class)
@SmallTest
@ExperimentalCoroutinesApi
@HiltAndroidTest

class BookDaoTest {

    val exceptions = mutableListOf<Throwable>()
    val customCaptor = CoroutineExceptionHandler { ctx, throwable ->
        exceptions.add(throwable) // add proper synchronization if the test is multithreaded
    }


    @get:Rule
    var instantTaskExecuterRule = InstantTaskExecutorRule()

    @Inject
    @Named("DataBaseTest")
    lateinit var dataBase: VieSureDataBase

    private lateinit var bookDao: BookDao

    @get:Rule
    var hiltRule = HiltAndroidRule(this)


    @Before
    fun setup() {
        hiltRule.inject()
        bookDao = dataBase.bookDataDao()
    }


    @After
    fun tearDown() {
        dataBase.close()
    }

    @Test
    fun insertBooksTest() {
        runBlocking {
            launch(customCaptor) {

                val bookEntitiesExcepted = listOf(
                    BookEntity(
                        1, "title", "desc", null, 1212, "imageurl"
                    )
                )

                val bookEntities = listOf(
                    BookEntity(
                        1, "title", "desc", "author", 1212, "imageurl"
                    )
                )
                bookDao.insertBooks(bookEntities)

                bookDao.getBooks().test {
                    val list = awaitItem()

                    Truth.assertThat(list).isEqualTo(bookEntitiesExcepted)
                    cancel()
                }
            }
        }
    }

    @Test
    fun getBookByIdTest() {
        runBlocking {
            launch(customCaptor) {

                val bookEntity = BookEntity(
                    1, "title", "desc", "author", 1212, "imageurl"
                )
                val bookEntities = listOf(
                    bookEntity
                )

                bookDao.insertBooks(bookEntities)

                bookDao.getBookById(1).test {
                    val list = awaitItem()
                    Truth.assertThat(list).isEqualTo(bookEntity)
                    cancel()

                }
            }
        }
    }
}