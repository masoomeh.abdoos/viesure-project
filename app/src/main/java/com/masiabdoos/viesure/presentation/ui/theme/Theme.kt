package com.masiabdoos.viesure.presentation.ui.theme

import LightColorScheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable


@Composable
fun ViesureTheme(content: @Composable () -> Unit) {
        MaterialTheme(
        colorScheme = LightColorScheme,
        typography = Typography,
        content = content,
        shapes = Shapes,
    )
}



