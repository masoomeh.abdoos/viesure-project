import androidx.compose.material3.lightColorScheme
import androidx.compose.ui.graphics.Color

val LightGray = Color(0xFFE7E6EB)
val Green = Color(0xFF03A9F4)

val LightColorScheme = lightColorScheme(
    primary = Green,
    secondary = Color.Yellow,
    background = Color.White,
    surface = Color.White,
    onPrimary = Color.White,
    onSecondary = Color.Black,
    onBackground = Color.Black,
    onSurface = Color.Black,
    primaryContainer = Color.Blue,
    secondaryContainer = Color.Green,
    surfaceVariant = LightGray,
    onSurfaceVariant = Color.DarkGray,
)
