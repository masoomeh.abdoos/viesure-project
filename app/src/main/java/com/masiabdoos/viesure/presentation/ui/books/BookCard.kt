package com.masiabdoos.viesure.presentation.ui.books

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.rememberAsyncImagePainter
import com.masiabdoos.viesure.R
import com.masiabdoos.viesure.domain.Book
import com.masiabdoos.viesure.presentation.ui.theme.Shapes
import com.masiabdoos.viesure.presentation.ui.theme.Typography
import com.masiabdoos.viesure.presentation.ui.theme.ViesureTheme

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun BookCard(
    book: Book, navigationToBookDetailScreen: (Long) -> Unit
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp),
        onClick = {
            navigationToBookDetailScreen(book.id)
        },
        shape = Shapes.medium,
        elevation = CardDefaults.cardElevation(defaultElevation = 4.dp),

        ) {
        Row(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth()
        ) {

            book.imageUrl?.let {
                val painter = rememberAsyncImagePainter(
                    model = it,
                    placeholder = painterResource(id = R.drawable.ic_launcher_background),
                    error = painterResource(id = R.drawable.image_loading_fail),

                    )
                Image(
                    modifier = Modifier
                        .size(64.dp)
                        .clip(RoundedCornerShape(50.dp)),
                    painter = painter,
                    contentDescription = book.title,
                    contentScale = ContentScale.Crop,
                )
            }

            Spacer(modifier = Modifier.width(16.dp))

            Column(
                modifier = Modifier.align(Alignment.CenterVertically),
                verticalArrangement = Arrangement.Center,
            ) {
                book.title?.let {
                    Text(
                        text = it,
                        style = Typography.bodyMedium,
                        fontWeight = FontWeight.Bold
                    )
                }
                Spacer(modifier = Modifier.height(8.dp))
                book.description?.let {
                    Text(
                        text = it, style = Typography.bodySmall, maxLines = 2
                    )
                }
            }
        }
    }
}


@Preview(showBackground = true)
@Composable
fun BookCardPreview() {
    ViesureTheme {
        val book = Book(
            id = 1,
            imageUrl = "https://dummyimage.com/400x300.png/cc0000/ffffff",
            title = "Item 1",
            description = "This is the description for item 1. It should be displayed in two lines."
        )
        BookCard(book = book, navigationToBookDetailScreen = {})
    }
}
