package com.masiabdoos.viesure.presentation.viewmode.bookdetail

import androidx.compose.runtime.Immutable
import com.masiabdoos.viesure.data.network.model.ErrorType
import com.masiabdoos.viesure.domain.BookDetail

// data class for Book  Detail page
@Immutable
data class BookDetailsState(
    val isLoading: Boolean = false,
    val book: BookDetail? = null,
    val error: ErrorType? = null,
    val isError: Boolean = false
)