package com.masiabdoos.viesure.presentation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.navigation.compose.rememberNavController
import com.masiabdoos.viesure.presentation.navigation.NavigationGraph
import com.masiabdoos.viesure.presentation.ui.theme.ViesureTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()

        setContent {
            //use them in the project
            ViesureTheme {
                //call the navigation graph
                val navController = rememberNavController()
                NavigationGraph(
                    navController = navController
                )
            }
        }

    }
}
