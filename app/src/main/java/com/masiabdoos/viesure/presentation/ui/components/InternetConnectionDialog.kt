package com.masiabdoos.viesure.presentation.ui.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import com.masiabdoos.viesure.R
import com.masiabdoos.viesure.presentation.ui.theme.Shapes
import com.masiabdoos.viesure.presentation.ui.theme.Typography
import com.masiabdoos.viesure.presentation.ui.theme.ViesureTheme


@Composable
fun InternetConnectionDialog(
    message: String, onRetry: () -> Unit, onCancel: () -> Unit
) {
    val shouldShowDialog = remember { mutableStateOf(true) }
    if (shouldShowDialog.value) {

        Dialog(
            onDismissRequest = {
                shouldShowDialog.value = false
                onCancel()
            }, properties = DialogProperties(usePlatformDefaultWidth = false)
        ) {
            Column(
                modifier = Modifier
                    .padding(8.dp)
                    .fillMaxWidth()
                    .background(
                        color = Color.White, shape = Shapes.large
                    )
                    .padding(horizontal = 10.dp),
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                Image(
                    modifier = Modifier.padding(8.dp),
                    painter = painterResource(id = R.drawable.ic_internet_connection),
                    contentDescription = "",

                    )

                Text(
                    modifier = Modifier.padding(8.dp),
                    text = message, style = Typography.bodyLarge,
                )

                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(start = 6.dp, end = 6.dp, top = 10.dp, bottom = 10.dp)
                ) {
                    OutlinedButton(
                        onClick = {
                            shouldShowDialog.value = false
                            onCancel()
                        },
                        modifier = Modifier
                            .fillMaxWidth()
                            .weight(1f)
                            .padding(horizontal = 6.dp),
//
                    ) {
                        Text(
                            stringResource(id = R.string.ok),
                            color = Color.Black,
                        )
                    }
                    Button(
                        onClick = {
                            onRetry()
                            shouldShowDialog.value = false
                        }, modifier = Modifier
                            .padding(horizontal = 16.dp)
                            .weight(1f)

                    ) {
                        Text(
                            text = stringResource(id = R.string.retry),
                            color = Color.White,
                        )
                    }
                }

            }
        }
    }

}

@Composable
@Preview
fun DeleteInvoiceDialogPreview() {
    ViesureTheme {
        InternetConnectionDialog(message = "internet error", onRetry = {}, onCancel = {})
    }
}
