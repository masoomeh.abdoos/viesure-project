package com.masiabdoos.viesure.presentation.ui.books

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.masiabdoos.viesure.presentation.viewmode.book.BookViewModel

@Composable
internal fun BooksScreen(
    viewModel: BookViewModel = hiltViewModel(),
    navigationToBookDetailScreen: (Long) -> Unit,
) {
    val state by viewModel.stateBooks.collectAsStateWithLifecycle()
    //for error message update ui
    val messageErrorState by viewModel.showMessageError.collectAsStateWithLifecycle()

    Box(
        modifier = Modifier
            .fillMaxHeight()
            .fillMaxWidth(),
        contentAlignment = Alignment.Center
    ) {
        BookContent(onRetry = { viewModel.retry() },
            bookState = state,
            messageErrorState = messageErrorState,
            navigationToBookDetailScreen = navigationToBookDetailScreen,
            updateErrorMessages = {
                viewModel.clearErrorMessage()
            })
    }

}