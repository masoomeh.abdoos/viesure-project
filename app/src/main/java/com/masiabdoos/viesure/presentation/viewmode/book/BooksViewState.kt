package com.masiabdoos.viesure.presentation.viewmode.book

import androidx.compose.runtime.Immutable
import com.masiabdoos.viesure.data.network.model.ErrorType
import com.masiabdoos.viesure.domain.Book

//ViewState
@Immutable
data class BooksViewState (
    val isLoading: Boolean =false,
    val books: List<Book> = emptyList(),
    val error: ErrorType? = null,
    val isError : Boolean =false,
    val isEmpty : Boolean =false,
)