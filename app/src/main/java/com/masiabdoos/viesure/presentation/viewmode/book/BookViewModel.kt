package com.masiabdoos.viesure.presentation.viewmode.book

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.masiabdoos.viesure.data.network.model.Response
import com.masiabdoos.viesure.data.repository.book.BookRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BookViewModel @Inject constructor(
    private val repository: BookRepository,

    ) : ViewModel() {

    private val _showMessageError = MutableStateFlow<Boolean>(false)
    val showMessageError = _showMessageError.asStateFlow()

    private var _stateBooks = MutableStateFlow(BooksViewState())
    val stateBooks = _stateBooks.asStateFlow()
    private fun updateState(bookState: BooksViewState) {
        _stateBooks.value = bookState
    }

    init {
        getBooks()
    }

    private fun getBooks() {
        viewModelScope.launch {

            repository.getBooks().collect { it ->

                when (it) {
                    is Response.Success -> {

                        val newStatusBook = stateBooks.value.copy(
                            books = it.data?.sortedByDescending {//sort by descending date
                                it.releaseDate
                            } ?: emptyList(),
                            isLoading = false
                        )
                        updateState(newStatusBook)
                    }

                    is Response.Error -> {
                        //I should send an error to the UI
                        setErrorMessage()
                        val newStatusBook = _stateBooks.value.copy(
                            isError = true,
                            error = it.network,
                            isLoading = false
                        )
                        updateState(newStatusBook)

                    }
                }
            }
        }
    }

    fun retry() {
        getBooks()
    }

    private fun setErrorMessage() {
        _showMessageError.value = true
    }

    fun clearErrorMessage() {
        _showMessageError.value = false
    }
}