package com.masiabdoos.viesure.presentation.viewmode.bookdetail

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.masiabdoos.viesure.data.repository.book.BookRepository
import com.masiabdoos.viesure.utils.Constants
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BookDetailsViewModel @Inject constructor(
    private val repository: BookRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {
    private var _stateBookDetail = MutableStateFlow(BookDetailsState())
    val stateBookDetail = _stateBookDetail.asStateFlow()
    private fun updateState(bookState: BookDetailsState) {
        _stateBookDetail.value = bookState
    }

    init {
        savedStateHandle.get<String>(Constants.BOOK_ID)?.let { bookId ->
            getBookDetail(bookId = bookId.toInt())
        }
    }

    private fun getBookDetail(bookId: Int) {
        updateState(BookDetailsState(isLoading = true))
        viewModelScope.launch {
            repository.getDetailBook(id = bookId).collect { bookDetail ->

                val newStatusBook = stateBookDetail.value.copy(
                    book = bookDetail,
                    isLoading = false
                )
                updateState(newStatusBook)
            }
        }
    }


}