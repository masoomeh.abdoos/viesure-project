package com.masiabdoos.viesure.presentation.ui.books

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.masiabdoos.viesure.domain.Book


@Composable
fun BookList(books: List<Book>, navigationToBookDetailScreen: (Long) -> Unit) {


    Scaffold(
//        topBar = {
//            TopAppBar(title = {})
//        },
        content = {

            Column(modifier = Modifier.padding(it)) {
                LazyColumn {
                    items(items = books, key = { book -> book.id }) { book ->
                        BookCard(
                            book = book, navigationToBookDetailScreen = navigationToBookDetailScreen
                        )
                    }
                }
            }
        },
    )
}

@Preview(showBackground = true)
@Composable
fun BookListPreview() {
    val sampleItems = listOf(
        Book(
            id = 1,
            imageUrl = "https://dummyimage.com/400x300.png/cc0000/ffffff",
            title = "Item 1",
            description = "This is the description for item 1. It should be displayed in two lines."
        ), Book(
            id = 2,
            imageUrl = "https://dummyimage.com/400x300.png/cc0000/ffffff",
            title = "Item 2",
            description = "This is the description for item 2. It should be displayed in two lines."
        )
    )
    BookList(books = sampleItems, navigationToBookDetailScreen = {})
}