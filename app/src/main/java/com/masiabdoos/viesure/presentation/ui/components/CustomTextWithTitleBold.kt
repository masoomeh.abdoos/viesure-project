package com.masiabdoos.viesure.presentation.ui.components

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview

@Composable
fun CustomTextWithTitleBold(textTitleBold: String, textDescNormal: String, modifier: Modifier) {
    Text(modifier = modifier, text = buildAnnotatedString {
        withStyle(style = SpanStyle(fontWeight = FontWeight.Bold)) {
            append(textTitleBold)
        }
        append(" ")
        append(textDescNormal)
    })
}

@Preview
@Composable
fun CustomTextWithTitleBoldPreview() {
    Text(
        text = buildAnnotatedString {
            withStyle(style = SpanStyle(fontWeight = FontWeight.Bold)) {
                append("textTitleBold")
            }
            append(" ")
            append("textDescNormal")
        },
        modifier = Modifier,
    )
}