package com.masiabdoos.viesure.presentation.ui.bookdetails

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import coil.compose.rememberAsyncImagePainter
import com.masiabdoos.viesure.R
import com.masiabdoos.viesure.domain.BookDetail
import com.masiabdoos.viesure.presentation.ui.components.CustomTextWithTitleBold
import com.masiabdoos.viesure.presentation.ui.theme.Typography
import com.masiabdoos.viesure.presentation.ui.theme.ViesureTheme


@Composable
fun BookDetailsContentPage(bookDetails: BookDetail) {
    val scrollState = rememberScrollState()

    Column(
        modifier = Modifier
            .background(color = Color.White)
            .fillMaxSize()
            .verticalScroll(scrollState)

    ) {

        ConstraintLayout(
            modifier = Modifier
                .background(color = Color.White)
                .fillMaxWidth()
                .wrapContentHeight()

        ) {
            val (photo, title, date, description, email) = createRefs()
            val startGuide = createGuidelineFromStart(0.05f)
            val endGuide = createGuidelineFromEnd(0.05f)

            val painter = rememberAsyncImagePainter(
                model = bookDetails.imageUrl,
                placeholder = painterResource(id = R.drawable.ic_launcher_background),
                error = painterResource(id = R.drawable.image_loading_fail),


                )
//
//
            Image(
                modifier = Modifier.constrainAs(photo) {
                    top.linkTo(parent.top)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                    width = Dimension.fillToConstraints
                    height = Dimension.ratio("1.5:1") // Ensure the image is  1.5:1 aspect ratio

                },
                painter = painter,
                contentDescription = bookDetails.title,
                contentScale = ContentScale.Crop,
            )

            Text(
                modifier = Modifier.constrainAs(title) {
                    top.linkTo(photo.bottom, margin = 6.dp)
                    start.linkTo(startGuide)
                    width = Dimension.fillToConstraints
                },
                text = bookDetails.title,
                fontWeight = FontWeight.Bold,
                style = Typography.bodyLarge,
            )

            Text(
                modifier = Modifier.constrainAs(date) {
                    top.linkTo(title.bottom, margin = 4.dp)
                    end.linkTo(endGuide)
                    bottom.linkTo(title.bottom)
                    width = Dimension.fillToConstraints
                },
                text = bookDetails.releaseDate,
                style = Typography.bodyMedium
            )

            Text(
                modifier = Modifier.constrainAs(description) {
                    top.linkTo(date.bottom, margin = 16.dp)
                    start.linkTo(startGuide)
                    end.linkTo(endGuide)
                    width = Dimension.fillToConstraints
                },
                text = bookDetails.description,
                style = Typography.bodyMedium,
            )


            CustomTextWithTitleBold(
                textTitleBold = stringResource(R.string.author),
                textDescNormal = bookDetails.author,
                modifier = Modifier.constrainAs(email) {
                    top.linkTo(description.bottom, margin = 16.dp)
                    start.linkTo(startGuide)
                    end.linkTo(endGuide)
                    width = Dimension.fillToConstraints
                },
            )
        }
    }
}


@Preview(showBackground = true)
@Composable
fun ContentPagePreview() {
    ViesureTheme {

        val bookDetail = BookDetail(
            id = 1,
            title = "The Hobbit",
            releaseDate = "1937",
            author = "J.R.R. Tolki",
            description = "Harper Lee'sqqqqqqqqqqqqqq timeless novel explores the racial injustices of the Deep South through the eyes of young Scout Finch. Set in the 1930s, the story centers around Scout's father, Atticus Finch, a lawyer who defends a black man, Tom Robinson, falsely accused of raping a white woman. The novel delves into themes of racial prejudice, moral growth, and the innocence of childhood. Scout, her brother Jem, and their friend Dill are fascinated by their reclusive neighbor, Boo Radley, who becomes a pivotal character in the story. Through the trial of Tom Robinson, the children witness the harsh realities of racism and injustice. Atticus Finch's unwavering integrity and sense of justice serve as a moral compass for the community. The novel also explores the complexities of social class and the coexistence of good and evil within individuals. Scout's journey from innocence to understanding is a poignant coming-of-age story. The novel's title refers to the idea that it is a sin to kill a mockingbird, symbolizing the destruction of innocence. The narrative is rich with symbolism and explores the importance of empathy and understanding. The character of Boo Radley represents the misunderstood and marginalized individuals in society. The book is a profound commentary on morality and human nature. Harper Lee's masterful storytelling and vivid characters make this a timeless classic. It remains a powerful and relevant exploration of racial and social issues. The novel won the Pulitzer Prize and has been translated into multiple languages",
            imageUrl = ""
        )
        BookDetailsContentPage(bookDetails = bookDetail)
    }

}