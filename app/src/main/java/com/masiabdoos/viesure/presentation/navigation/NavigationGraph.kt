package com.masiabdoos.viesure.presentation.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NamedNavArgument
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.masiabdoos.viesure.presentation.ui.bookdetails.BookDetailsScreen
import com.masiabdoos.viesure.presentation.ui.books.BooksScreen
import com.masiabdoos.viesure.utils.Constants

@Composable
fun NavigationGraph(
    navController: NavHostController,
) {
    NavHost(
        navController = navController, startDestination = NavigationScreens.Books.screenRoute
    ) {
        composable(NavigationScreens.Books.screenRoute) {
            BooksScreen(navigationToBookDetailScreen = { bookId ->
                //pass id to book details screen
                navController.navigate("${NavigationScreens.BookDetails.screenRoute}/${bookId}")
            })
        }
        composable(
            NavigationScreens.BookDetails.screenRoute + "/{${Constants.BOOK_ID}}",
            arguments = listOf(navArgument(name = Constants.BOOK_ID) { NavType.IntType })
        ) {
            BookDetailsScreen()
        }
    }
}

//define navigation screens for navigation graph
sealed class NavigationScreens(var screenRoute: String, val arguments: List<NamedNavArgument>) {
    data object Books : NavigationScreens(Constants.BOOKS, emptyList())
    data object BookDetails : NavigationScreens(
        Constants.BOOK_DETAILS, listOf(navArgument(name = Constants.BOOK_ID) { NavType.IntType })
    )
}