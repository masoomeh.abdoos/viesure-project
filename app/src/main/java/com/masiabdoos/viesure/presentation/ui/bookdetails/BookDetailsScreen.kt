package com.masiabdoos.viesure.presentation.ui.bookdetails

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.masiabdoos.viesure.presentation.viewmode.bookdetail.BookDetailsViewModel

@Composable
internal fun BookDetailsScreen(viewModel: BookDetailsViewModel = hiltViewModel()) {

    val state by viewModel.stateBookDetail.collectAsStateWithLifecycle()

    Box(
         modifier = Modifier
            .fillMaxHeight()
            .fillMaxWidth(),
        contentAlignment = Alignment.Center,
    ) {
        state.book?.let { BookDetailsContentPage(bookDetails = it) }
    }

}