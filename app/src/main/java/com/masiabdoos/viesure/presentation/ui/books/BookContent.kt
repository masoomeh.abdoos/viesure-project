package com.masiabdoos.viesure.presentation.ui.books

import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import com.masiabdoos.viesure.R
import com.masiabdoos.viesure.data.network.model.ErrorType
import com.masiabdoos.viesure.presentation.ui.components.EmptyContent
import com.masiabdoos.viesure.presentation.ui.components.InternetConnectionDialog
import com.masiabdoos.viesure.presentation.viewmode.book.BooksViewState


@Composable
fun BookContent(
    onRetry: () -> Unit,
    bookState: BooksViewState,
    messageErrorState: Boolean,
    navigationToBookDetailScreen: (Long) -> Unit,
    updateErrorMessages: () -> Unit
) {


    if (bookState.isEmpty) {
        //show empty state
        EmptyContent()
    }

    if (bookState.isError && messageErrorState) {
        when (bookState.error) {
            ErrorType.Api.Network -> {
                //show the image for disconnected

                InternetConnectionDialog(message = stringResource(
                    id = R.string.check_internet_connection_message
                ), onRetry = { onRetry() }, onCancel = {

                    updateErrorMessages()
                })

            }

            else -> {
                //unexpected error
            }
        }
    }
    BookList(
        books = bookState.books,
        navigationToBookDetailScreen = navigationToBookDetailScreen
    )


}


