package com.masiabdoos.viesure.di.database

import android.content.Context
import androidx.room.Room
import com.masiabdoos.viesure.data.database.VieSureDataBase
import com.masiabdoos.viesure.utils.Constants.Companion.DATABASE_NAME
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object DataBaseModule {

    @Provides
    @Singleton
    fun provideDataBase(@ApplicationContext context: Context) =
        Room.databaseBuilder(context, VieSureDataBase::class.java, DATABASE_NAME)
            .build()


    @Provides
    @Singleton
    fun provideBookDao(database: VieSureDataBase) = database.bookDataDao()

}