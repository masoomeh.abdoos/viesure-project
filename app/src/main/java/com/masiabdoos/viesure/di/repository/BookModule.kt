package com.masiabdoos.viesure.di.repository

import com.masiabdoos.viesure.data.database.entities.BookEntity
import com.masiabdoos.viesure.data.map.BookApiToBookMapper
import com.masiabdoos.viesure.data.map.BookDetailsMapper
import com.masiabdoos.viesure.data.map.BookApiToBookEntityMapper
import com.masiabdoos.viesure.data.map.BookMapper
import com.masiabdoos.viesure.data.map.Mapper
import com.masiabdoos.viesure.data.network.model.BookApiModel
import com.masiabdoos.viesure.data.repository.book.BookRepository
import com.masiabdoos.viesure.data.repository.book.BookRepositoryImpl
import com.masiabdoos.viesure.domain.Book
import com.masiabdoos.viesure.domain.BookDetail
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class BookModule {

    @Binds
    abstract fun bindBookUiMapper(bookDataMapper: BookMapper):
            Mapper<BookEntity, Book>

    @Binds
    abstract fun bindBookApiModelToBookEntityMapper(bookEntityDataMapper: BookApiToBookEntityMapper):
            Mapper<BookApiModel, BookEntity>


    @Binds
    abstract fun bindBookDetailsUiMapper(bookDetailsDataMapper: BookDetailsMapper):
            Mapper<BookEntity, BookDetail>

    @Binds
    abstract fun bindBookApiToBooksUiMapper(bookDetailsDataMapper: BookApiToBookMapper):
            Mapper<BookApiModel, Book>

    @Binds
    @Singleton
    abstract fun bindBookRepository(bookRepositoryImpl: BookRepositoryImpl): BookRepository


}
