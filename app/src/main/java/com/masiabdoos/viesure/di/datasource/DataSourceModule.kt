package com.masiabdoos.viesure.di.datasource

import com.masiabdoos.viesure.data.source.local.BookLocalDataSource
import com.masiabdoos.viesure.data.source.local.BookLocalDataSourceImpl
import com.masiabdoos.viesure.data.source.remote.BookRemoteDataSource
import com.masiabdoos.viesure.data.source.remote.BookRemoteDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

// DataSourceModule
// Provides the Local and Remote data sources
@Module
@InstallIn(SingletonComponent::class)
abstract class DataSourceModule {

    @Binds
    @Singleton
    abstract fun bindBookLocalDataSource(bookLocalDataSourceImpl: BookLocalDataSourceImpl):
            BookLocalDataSource

    @Binds
    @Singleton
    abstract fun bindBookRemoteDataSource(bookRemoteDataSourceImpl: BookRemoteDataSourceImpl):
            BookRemoteDataSource


}