package com.masiabdoos.viesure.utils

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import java.util.TimeZone

object Utils {
    /**
     * Parses a string date into a long time.
     * convert string date of format MM/dd/yyyy to long time
     */
    fun parseStringDateToLong(inputDate: String?): Long {

        if (inputDate.isNullOrEmpty()) {
            return 0
        }
        try {

            // Parse the input date string into a Date object
            val dateFormat = SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH)
            dateFormat.timeZone = TimeZone.getTimeZone("UTC")
            val date: Date? = dateFormat.parse(inputDate)

            return date?.time ?: 0

        } catch (e: ParseException) {
            e.printStackTrace()
            return 0
        }
    }

    /**
     * validation string date and reformat to MM/dd/yyyy
     */
    fun reformatDate(inputDate: String?): String {
        val regex = Regex("(\\d{1,2})/(\\d{1,2})/(\\d{4})")
        if (inputDate.isNullOrEmpty() || !regex.matches(inputDate)) {

            return ""
        }
        return inputDate.let {
            regex.replace(it) { matchResult ->
                val (month, day, year) = matchResult.destructured
                "%02d/%02d/%04d".format(month.toInt(), day.toInt(), year.toInt())
            }
        }
    }

    /**
     * Reformats a date string from MM/dd/yyyy to EEE, MMM d, ''yy from long time
     *
     */
    fun parseLongToFormattedDateString(longTime: Long): String {
        if (longTime == 0L) {
            return ""
        }
        // Create a Date object from the long time
        val date = Date(longTime)
        val dateFormat = SimpleDateFormat("EEE, MMM d, ''yy", Locale.ENGLISH)
        val formattedDate = dateFormat.format(date)
        return formattedDate
    }


}