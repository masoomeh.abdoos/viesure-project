package com.masiabdoos.viesure.utils
class Constants {
    companion object {
        const val BOOK_ID: String = "bookId"
        const val BOOKS: String = "books"
        const val BOOK_DETAILS: String = "book_details/{book_id}"

        const val BASE_URL: String = "https://c27b2d72-8d9c-4aa0-b549-7ae7e5666815.mock.pstmn.io/"
        const val DATABASE_NAME: String = "viesure.db"
        const val BOOK_TABLE: String = "book_table"
    }


}