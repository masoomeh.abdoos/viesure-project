package com.masiabdoos.viesure

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ViesureApplication: Application() {

}