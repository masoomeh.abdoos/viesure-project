package com.masiabdoos.viesure.domain

data class Book(
    var id: Long,
    var title: String? = null,
    var imageUrl: String? = null,
    var description: String? = null,
    var releaseDate: Long? = 0,
)
