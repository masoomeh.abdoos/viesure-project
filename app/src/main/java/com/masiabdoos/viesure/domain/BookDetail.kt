package com.masiabdoos.viesure.domain

data class BookDetail(
    var id: Long,
    var title: String,
    var description: String,
    var author: String,
    var imageUrl: String,
    var releaseDate: String,
)
