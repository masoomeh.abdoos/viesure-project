package com.masiabdoos.viesure.data.network.api

import okhttp3.Interceptor
import okhttp3.Response
import java.lang.Thread.sleep

//retry 3 times with 2 seconds backoff delay
class RetryInterceptor : Interceptor {

    private var maxRetry = 3  //retry 3 times
    private var retryDelayMs: Long = 2000 //for 2-second delay
    private var retryCount = 0

    override fun intercept(chain: Interceptor.Chain): Response {

        val request = chain.request()
        var response = chain.proceed(request)

        // Retry the request if response code is 500
        while (!response.isSuccessful && retryCount < maxRetry && response.code == 500) {
            retryCount++
            sleep(retryDelayMs)

            response.close()
            response = chain.proceed(request)
        }

        return response
    }
}