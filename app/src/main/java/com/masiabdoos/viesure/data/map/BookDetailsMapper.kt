package com.masiabdoos.viesure.data.map

import com.masiabdoos.viesure.data.database.entities.BookEntity
import com.masiabdoos.viesure.domain.BookDetail
import com.masiabdoos.viesure.utils.Utils
import javax.inject.Inject
//Mapper BookEntity to BookDetail
class BookDetailsMapper @Inject constructor() : Mapper<BookEntity, BookDetail> {

    override fun map(input: BookEntity): BookDetail {
        return BookDetail(
            id = input.id,
            title = input.title,
            imageUrl = input.imageUrl,
            description = input.description,
            author = input.author ?: "",
            releaseDate = Utils.parseLongToFormattedDateString(input.releaseDate)
        )
    }
}