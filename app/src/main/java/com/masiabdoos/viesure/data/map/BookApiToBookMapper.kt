package com.masiabdoos.viesure.data.map

import com.masiabdoos.viesure.data.network.model.BookApiModel
import com.masiabdoos.viesure.domain.Book
import com.masiabdoos.viesure.utils.Utils
import javax.inject.Inject

class BookApiToBookMapper @Inject constructor() : Mapper<BookApiModel, Book> {

    override fun map(input: BookApiModel): Book {
        return Book(
            id = input.id ?: 0,
            title = input.title,
            imageUrl = input.image,
            description = input.description,
            releaseDate = Utils.parseStringDateToLong(
                Utils.reformatDate(input.releaseDate)
            ),
        )
    }
}