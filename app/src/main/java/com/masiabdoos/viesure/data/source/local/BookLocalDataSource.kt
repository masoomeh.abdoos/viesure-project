package com.masiabdoos.viesure.data.source.local

import com.masiabdoos.viesure.data.database.entities.BookEntity
import kotlinx.coroutines.flow.Flow

interface BookLocalDataSource {
    suspend fun getBooks(): Flow<List<BookEntity>>
    suspend fun getBookDetails(id: Int): Flow<BookEntity>
    suspend fun insertBooks(booksEntity: List<BookEntity>)
}