package com.masiabdoos.viesure.data.repository.book

import com.masiabdoos.viesure.data.network.model.Response
import com.masiabdoos.viesure.domain.Book
import com.masiabdoos.viesure.domain.BookDetail
import kotlinx.coroutines.flow.Flow

interface BookRepository {
    suspend fun getBooks(): Flow<Response<List<Book>>>
//    suspend fun insertBooks(books: List<BookEntity>)
    suspend fun getDetailBook(id: Int): Flow<BookDetail>
//    suspend fun getBooks(): Flow<List<Book>>
}