package com.masiabdoos.viesure.data.network.api

import com.masiabdoos.viesure.data.network.model.Response
import com.masiabdoos.viesure.data.network.model.toNetworkError
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

//handle error and success response from Api service
object Call {
    fun <T> safeCall(
        dispatcher: CoroutineDispatcher = Dispatchers.IO, apiCall: suspend () -> T
    ): Flow<Response<T>> {

        return flow {
            try {
                val result = apiCall.invoke()
                emit(Response.Success(result)) //return success response with data
            } catch (throwable: Throwable) {
                throwable.printStackTrace()
                emit(Response.Error(throwable.toNetworkError())) //return error response with specific error
            }
        }.flowOn(dispatcher)
    }
}
