package com.masiabdoos.viesure.data.network.api

import com.masiabdoos.viesure.data.network.model.BookApiModel
import retrofit2.http.GET


/**
 * Retrofit Api service interface
 */
interface ViesureServiceApi {
    @GET(".")
    suspend fun getBooks(): List<BookApiModel> // get list of books

}