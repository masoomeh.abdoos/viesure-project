package com.masiabdoos.viesure.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.masiabdoos.viesure.data.database.dao.BookDao
import com.masiabdoos.viesure.data.database.entities.BookEntity

@Database(
    entities = [BookEntity::class], version = 2, exportSchema = false
)
abstract class VieSureDataBase : RoomDatabase() {

    abstract fun bookDataDao(): BookDao

}