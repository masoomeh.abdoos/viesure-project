package com.masiabdoos.viesure.data.network.model

// Error codes
//handle http errors code
object ErrorCodes {
    object Http {
        const val InternalServer = 500
        const val ServiceUnavailable = 503
        const val ResourceNotFound = 404
    }
}