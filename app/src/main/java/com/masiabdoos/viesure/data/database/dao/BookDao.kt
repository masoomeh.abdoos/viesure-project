package com.masiabdoos.viesure.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.masiabdoos.viesure.data.database.entities.BookEntity
import kotlinx.coroutines.flow.Flow


@Dao
interface BookDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertBooks(booksEntity: List<BookEntity>): List<Long>

    @Query("SELECT id,title,description,image_url,release_date from book_table")
    fun getBooks(): Flow<List<BookEntity>>

    @Query("SELECT * FROM book_table WHERE id = :bookId")
    fun getBookById(bookId: Int): Flow<BookEntity>

}


