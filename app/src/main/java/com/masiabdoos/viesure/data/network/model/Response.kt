package com.masiabdoos.viesure.data.network.model

//Response data class for API calls
sealed class Response<T>(
    val data: T? = null, val network: ErrorType? = null
) {
    class Success<T>(data: T) : Response<T>(data)
    class Error<T>(network: ErrorType?, data: T? = null) : Response<T>(data, network)
}