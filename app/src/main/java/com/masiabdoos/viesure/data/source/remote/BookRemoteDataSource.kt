package com.masiabdoos.viesure.data.source.remote

import com.masiabdoos.viesure.data.network.model.BookApiModel
import com.masiabdoos.viesure.data.network.model.Response
import kotlinx.coroutines.flow.Flow

interface BookRemoteDataSource {
    //get books
    suspend fun getBooks(): Flow<Response<List<BookApiModel>>>
}