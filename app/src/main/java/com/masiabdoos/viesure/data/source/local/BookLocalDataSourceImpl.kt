package com.masiabdoos.viesure.data.source.local

import com.masiabdoos.viesure.data.database.dao.BookDao
import com.masiabdoos.viesure.data.database.entities.BookEntity
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class BookLocalDataSourceImpl @Inject constructor(
    private val bookDao: BookDao,
) : BookLocalDataSource {
    override suspend fun getBooks(): Flow<List<BookEntity>> {
        return bookDao.getBooks()
    }

    override suspend fun getBookDetails(id: Int): Flow<BookEntity> {
        return bookDao.getBookById(id)
    }

    override suspend fun insertBooks(bookEntities: List<BookEntity>) {
        bookDao.insertBooks(bookEntities)
    }
}