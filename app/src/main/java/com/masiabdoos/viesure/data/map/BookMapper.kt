package com.masiabdoos.viesure.data.map

import com.masiabdoos.viesure.data.database.entities.BookEntity
import com.masiabdoos.viesure.domain.Book
import javax.inject.Inject

//Mapper BookEntity to Book
class BookMapper @Inject constructor() : Mapper<BookEntity, Book> {

    override fun map(input: BookEntity): Book {
        return Book(input.id, input.title, input.imageUrl, input.description,input.releaseDate)
    }
}