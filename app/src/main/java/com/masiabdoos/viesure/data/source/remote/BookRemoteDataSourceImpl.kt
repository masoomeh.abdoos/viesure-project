package com.masiabdoos.viesure.data.source.remote

import com.masiabdoos.viesure.data.network.api.Call
import com.masiabdoos.viesure.data.network.api.ViesureServiceApi
import javax.inject.Inject

//BookRemoteDataSource for getting data from remote source (API)
class BookRemoteDataSourceImpl @Inject constructor(
    private val bookService: ViesureServiceApi,
) : BookRemoteDataSource {
    override suspend fun getBooks() = Call.safeCall {
        bookService.getBooks()
    }
}