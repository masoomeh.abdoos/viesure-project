package com.masiabdoos.viesure.data.repository.book

import com.masiabdoos.viesure.data.database.entities.BookEntity
import com.masiabdoos.viesure.data.map.Mapper
import com.masiabdoos.viesure.data.network.model.BookApiModel
import com.masiabdoos.viesure.data.network.model.Response
import com.masiabdoos.viesure.data.source.local.BookLocalDataSource
import com.masiabdoos.viesure.data.source.remote.BookRemoteDataSource
import com.masiabdoos.viesure.domain.Book
import com.masiabdoos.viesure.domain.BookDetail
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

//book repository handle get data from local or remote and map data to domain model
class BookRepositoryImpl @Inject constructor(
    private val bookRemoteDataSource: BookRemoteDataSource,
    private val bookLocalDataSource: BookLocalDataSource,
    private val bookDataMapper: Mapper<BookEntity, Book>,//map book entity data to book mode
    private val bookEntityDataMapper: Mapper<BookApiModel, BookEntity>, //map book api model to book entity
    private val bookApiModelToBookDataMapper: Mapper<BookApiModel, Book>, //map book api model to book entity
    private val bookDetailsDataMapper: Mapper<BookEntity, BookDetail>

) : BookRepository {
    override suspend fun getBooks(): Flow<Response<List<Book>>> = flow {
        bookRemoteDataSource.getBooks().collect { response ->
            when (response) {
                is Response.Error -> {//error
                    emit(Response.Error(response.network))

                    bookLocalDataSource.getBooks().catch { it ->
//                        emit(Response.Error(it.toNetworkError()))
                    }.collect { bookEntities ->
                        val data = bookEntities.map {
                            bookDataMapper.map(it)
                        }
                        emit(Response.Success(data))
                    }
                }

                is Response.Success -> {//data

                    val dataMapper = response.data!!.map {
                        bookApiModelToBookDataMapper.map(it)
                    }
                    emit(Response.Success(dataMapper))

                    //insert data to local for updating the data
                    insertBookToDb(response.data)

                }
            }
        }
    }

    //insert data to local
    private suspend fun insertBookToDb(data: List<BookApiModel>) {
        //map data to entity
        val dataMapper = data.map {
            bookEntityDataMapper.map(it)
        }
        return bookLocalDataSource.insertBooks(booksEntity = dataMapper)

    }


    //get book details by id
    override suspend fun getDetailBook(id: Int): Flow<BookDetail> {
        return bookLocalDataSource.getBookDetails(id = id).map {
            bookDetailsDataMapper.map(it)

        }
    }

}