package com.masiabdoos.viesure.data.network.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

//data model of book for getting from server
@Serializable
data class BookApiModel(
    @SerialName("id")
    var id: Long? = null,
    @SerialName("title")
    var title: String? = null,
    @SerialName("description")
    var description: String? = null,
    @SerialName("author")
    var author: String? = null,
    @SerialName("release_date")
    var releaseDate: String? = null,
    @SerialName("image")
    var image: String? = null,


    )