package com.masiabdoos.viesure.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.masiabdoos.viesure.utils.Constants.Companion.BOOK_TABLE

@Entity(tableName = BOOK_TABLE)
data class BookEntity(
    @PrimaryKey val id: Long,
    @ColumnInfo(name = "title") val title: String,
    @ColumnInfo(name = "description") val description: String,
    @ColumnInfo(name = "author") val author: String? = null,
    @ColumnInfo(name = "release_date") val releaseDate: Long,
    @ColumnInfo(name = "image_url") val imageUrl: String
)