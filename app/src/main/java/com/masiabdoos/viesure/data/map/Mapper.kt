package com.masiabdoos.viesure.data.map

//mapping data
interface Mapper<I, O> {
    fun map(input: I): O
}