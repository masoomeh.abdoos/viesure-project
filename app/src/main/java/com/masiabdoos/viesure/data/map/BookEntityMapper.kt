package com.masiabdoos.viesure.data.map

import com.masiabdoos.viesure.data.database.entities.BookEntity
import com.masiabdoos.viesure.data.network.model.BookApiModel
import com.masiabdoos.viesure.utils.Utils
import javax.inject.Inject

// Mapper bookApiModel to bookEntity
class BookEntityMapper @Inject constructor() : Mapper<BookApiModel, BookEntity> {
    override fun map(input: BookApiModel): BookEntity {
        return BookEntity(
            id = input.id ?: 0,
            title = input.title ?: "",
            imageUrl = input.image ?: "",
            description = input.description ?: "",
            releaseDate = Utils.parseStringDateToLong(
                Utils.reformatDate(input.releaseDate)
            ),
            author = input.author ?: "",
        )
    }

}