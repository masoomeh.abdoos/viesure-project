## VieSure
![Project Screenshots](https://gitlab.com/masoomeh.abdoos/vieSure/-/raw/main/screen_shoot.jpg)

## Getting started

This Android app has two main screens: a list of books and a detailed page for each book. It fetches data from an API, displaying the books on the first screen. Selecting a book navigates to a detailed view with more information.

## Features

Book List Screen:
* Displays a list of books with their titles, short descriptions, and images.
* Fetches book data from an API or database.
* If the internet connection is unavailable, fetches data from the local database and shows a dialog box informing the user.
* Sorts the list by release date.
* If the request encounters an internal server error, it retries up to 3 times with a 2-second backoff delay using an interceptor.

Book Detail Screen:
* Shows detailed information about the selected book, including the image, title, release date, description, and author.
* Displays the release date in a special format (e.g., "Wed, Jul 8, '20").



## Tools and Technologies Used
* Architecture: MVVM (Model-View-ViewModel)
* Retrofit
* Coil
* flow
* JetpackCompose
* Navigation Compose
* JUni


## Project Structure
```
com.masiabdoos.viesure
├── ViesureApplication.kt
├── data
│   ├── database
│   │   ├── VieSureDataBase.kt
│   │   ├── dao
│   │   └── entities
│   ├── map
│   │   
│   ├── network
│   │   ├── api
│   │   └── model
│   ├── repository
│   │   └── book
│   └── source
│       ├── local
│       └── remote
├── di
│   ├── coroutine
│   ├── database
│   ├── datasource
│   ├── network 
│   └── repository
│       
├── domain
│  
├── presentation
│   ├── MainActivity.kt
│   ├── navigation
│   ├── ui
│   │   ├── bookdetails
│   │   ├── books
│   │   ├── components
│   │   └── theme
│   └── viewmode
│       ├── book
│       └── bookdetail
└── utils
```



